#include "command_line_arguments.h"
#include "gzfile.h"
#include <iostream>
#include <boost/filesystem.hpp>
#include <cstdlib>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

boost::program_options::variables_map command_line_arguments(int argc, char ** argv) {
  
  boost::program_options::variables_map global_args;

  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
  
    // MISC ARGUMENTS
  
    ( "help", 
      "print this message")
      
    ( "version", 
      "print version information")
      
    // REQUIRED ARGUMENTS

    ( "sample",
      boost::program_options::value<std::string>()->default_value(""),
      "path to input file in VCF format. May be gz-compressed.")
      
    ( "samples",
      boost::program_options::value<std::vector<std::string>>()->multitoken(),
      "path to multiple input files in plink dosage format. May be gz-compressed.")
      
    ( "reference",
      boost::program_options::value<std::string>()->required(),
      "path to reference file in VCF format. May be gz-compressed.")
      
    ( "genes", 
      boost::program_options::value<std::string>()->required(),
      "path to gene database in Gene Transfer Format. May be gz-compressed.")

    ( "out", 
      boost::program_options::value<std::string>()->required(),
      "filename for writeable output file. Any existing file will be overwritten.")
      
    // OPTIONAL ARGUMENTS
      
    ( "flank", 
      boost::program_options::value<std::size_t>()->default_value(0),
      "symmetrical flanking region in kb")
      
    ( "pre-flank", 
      boost::program_options::value<std::size_t>()->default_value(0),
      "pre-gene asymmetrical flanking region in kb")
      
    ( "post-flank", 
      boost::program_options::value<std::size_t>()->default_value(0),
      "post-gene asymmetrical flanking region in kb")
    
    ( "maf",
      boost::program_options::value<double>()->default_value(0.01),
      "minor allele frequency threshold")
      
    ( "maf-ratio",
      boost::program_options::value<double>()->default_value(0.33),
      "permitted threshold between sample and reference maf beween 0-1."
      "smaller values are more strict"
    )
      
    ( 
      "gtf-filter",
      boost::program_options::value<std::vector<std::string>>()->multitoken(),
      "key-value pair"
    )
    
    ( 
      "include-snps",
      boost::program_options::value<std::string>(),
      "file name to whitespace-separated snp-names for snps to include"
      "if provided, snps not in the file will be excluded"
    )
    
    ( 
      "exclude-snps",
      boost::program_options::value<std::string>(),
      "file name to whitespace-separated snp-names for snps to exclude"
      "if provided, snps in the file will be excluded"
    )
    
    ( "delimiter",
      boost::program_options::value<std::string>(),
      "replace comma seperator with user-defined one, e.g. --sep \";\""
      ". Must consist of a single character"
    )

    ( "zscore", 
      "output z-transformed scores instead of raw numbers")
      
    ( "hard-calls", 
      "force the program to use hard call (GT) data")
    
    ( "verbose", 
      "print extra information during run")
      
    ( "trace", 
      "print A LOT of extra information during run. "
      "This will slow down the program. Implies --verbose")
  ;
  
  boost::program_options::store(
    boost::program_options::parse_command_line(argc, argv, desc),
    global_args
  );
  
  std::cout << "PFGS " << PFGS_VERSION << '\n';
  
  if (global_args.count("help")) {
      std::cout << desc << '\n';
      std::exit(EXIT_SUCCESS);
  }
  
  if (global_args.count("version")) {
    std::exit(EXIT_SUCCESS);
  }

  try {
    boost::program_options::notify(global_args);
  }
  catch (boost::program_options::required_option const & e) {
    std::cerr << "Command line options error:\n";
    std::cerr << "  " << e.what() << '\n';
    std::cerr << "Use the --help option for more information.\n";
    std::exit(EXIT_FAILURE);
  }
  
  if (global_args.count("gtf-filter")) {
    auto const filter_vec = global_args["gtf-filter"].as<std::vector<std::string>>();
    for (auto const & str : filter_vec) {
      std::size_t const eq_idx = str.find('=');
      if (eq_idx == std::string::npos) {
        std::cerr << "Command line options error:\n";
        std::cerr << "Cannot parse --gtf-filter option `" << str << "`.\n";
        std::cerr << "Please use `key=value` notation; see --help for more information.\n";
        std::exit(EXIT_FAILURE);
      }
    }
  }
  
  if (global_args.count("include-snps") && global_args.count("exclude-snps")) {
    std::cerr << "The options --include-snps and --exclude-snps"
                 " cannot both be provided at the same time.\n";
    std::exit(EXIT_FAILURE);
  }
  
  if (global_args["flank"].as<std::size_t>() && 
       (global_args["pre-flank"].as<std::size_t>() || global_args["post-flank"].as<std::size_t>())) {
    std::cerr << "The options --flank and --pre-flank/--post-flank "
                 " cannot both be provided at the same time.\n";
    std::exit(EXIT_FAILURE);
  }

  std::cout << "Execution dir: " << boost::filesystem::current_path() << '\n';
  std::cout << "--sample " << global_args["sample"].as<std::string>() << '\n';
  std::cout << "--reference " << global_args["reference"].as<std::string>() << '\n';
  std::cout << "--genes " << global_args["genes"].as<std::string>() << '\n';
  std::cout << "--out " << global_args["out"].as<std::string>() << '\n';
  std::cout << "--maf " << global_args["maf"].as<double>() << '\n';
  std::cout << "--maf-ratio " << global_args["maf-ratio"].as<double>() << '\n';
  
  if (global_args["flank"].as<std::size_t>())
    std::cout << "--flank " << global_args["flank"].as<std::size_t>() << '\n';
    
  if (global_args["pre-flank"].as<std::size_t>())
    std::cout << "--pre-flank " << global_args["pre-flank"].as<std::size_t>() << '\n';
    
  if (global_args["post-flank"].as<std::size_t>())
    std::cout << "--post-flank " << global_args["post-flank"].as<std::size_t>() << '\n';
  
  if (global_args.count("delimiter"))
    std::cout << "--delimiter " << global_args["delimiter"].as<std::string>() << '\n';

  if (global_args.count("zscore"))
    std::cout << "--zscore\n";


  if (global_args.count("hard-calls"))
    std::cout << "--hard-calls\n";
  
  if (global_args.count("verbose"))
    std::cout << "--verbose\n";

  if (global_args.count("trace"))
    std::cout << "--trace\n";

  if (global_args.count("gtf-filter"))
    for (auto const & fitem : global_args["gtf-filter"].as<std::vector<std::string>>())
      std::cout << "--gtf-filter " << fitem << '\n';
      
  if (global_args.count("include-snps"))
    std::cout << "--include-snps " << global_args["include-snps"].as<std::string>() << '\n';
  
  if (global_args.count("exclude-snps"))
    std::cout << "--exclude-snps " << global_args["exclude-snps"].as<std::string>() << '\n';

  // set logging
  auto severity = boost::log::trivial::warning;
  if (global_args.count("trace"))
    severity = boost::log::trivial::trace;
  else if (global_args.count("verbose"))
    severity = boost::log::trivial::info;

  
  boost::log::core::get()->set_filter
  (
    boost::log::trivial::severity >= severity
  );

  std::cout << '\n';
  
  return global_args;
}

std::unordered_map<std::string, std::string> create_gtf_filter(boost::program_options::variables_map const & global_args) {
  std::unordered_map<std::string, std::string> filter;
  if (!global_args.count("gtf-filter"))
    return filter;
    
  auto const filter_vec = global_args["gtf-filter"].as<std::vector<std::string>>();
  for (auto const & str : filter_vec) {
    std::size_t const eq_idx = str.find('=');
    filter[str.substr(0, eq_idx)] = str.substr(eq_idx + 1);
  }

  return filter;
}

void read_snp_file(std::string const & fname, std::unordered_map<std::string, bool> & out, bool value) {
  GZfile file(fname);
  if (!file)
    BOOST_LOG_TRIVIAL(warning) << "Cannot open file `" << fname << "`. No loci read.";
  std::string loc;
  while (file.handle() >> loc)
    out[std::move(loc)] = value;
  if (out.empty())
    BOOST_LOG_TRIVIAL(warning) << "No loci read from file `" << fname << "`.";
  BOOST_LOG_TRIVIAL(info) << "Read " << out.size() << " loci from `" << fname << "`.";
}

std::unordered_map<std::string, bool> create_snp_filter(boost::program_options::variables_map const & global_args) {
  std::unordered_map<std::string, bool> out;
  if (global_args.count("include-snps"))
    read_snp_file(global_args["include-snps"].as<std::string>(), out, true);
  else if (global_args.count("exclude-snps"))
    read_snp_file(global_args["exclude-snps"].as<std::string>(), out, false);
  return out;
}


