#ifndef INCL_COMMAND_LINE_ARGUMENTS_H
#define INCL_COMMAND_LINE_ARGUMENTS_H

#define PFGS_VERSION "0.930-multi_chr_file"

#include <boost/program_options.hpp>
#include <unordered_map>

boost::program_options::variables_map command_line_arguments(int, char **);

std::unordered_map<std::string, std::string> create_gtf_filter(boost::program_options::variables_map const &);

std::unordered_map<std::string, bool> create_snp_filter(boost::program_options::variables_map const &);

#endif
