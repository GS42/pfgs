#include "gene_score.h"
#include <iostream>
#include <boost/log/trivial.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
#include <cmath>
#include <iterator>
#include <numeric>

std::vector<Gene_score> gene_score(
  SNPreader::Locus const & sample_locus, 
  SNPreader::Locus const & reference_locus)
{
	BOOST_LOG_TRIVIAL(trace) << "Starting gene score "
    "DS calculation for current locus.";
	
	std::vector<Gene_score> out;
	out.resize(sample_locus.data_ds.size(), 0);

	std::unordered_map<long double, Gene_score> cache;

	for (std::size_t sample_idx = 0; sample_idx < sample_locus.data_ds.size(); ++sample_idx) {
		
		auto const dosage = sample_locus.data_ds[sample_idx];

		// check to see if dosage is in cache
		auto itt = cache.find(dosage);
		if (itt != cache.end()) {
		  out[sample_idx] = itt->second;
		  continue;
		}

		// else, calculate
		Gene_score gscore = 0;
		for (std::size_t reference_idx = 0; reference_idx < reference_locus.data_ds.size(); ++reference_idx) {
		  gscore += dosage > reference_locus.data_ds[reference_idx] ? 
        dosage - reference_locus.data_ds[reference_idx]
          :
        reference_locus.data_ds[reference_idx] - dosage;
	  }

	  cache[dosage] = gscore;
		out[sample_idx] = gscore;
  }

	BOOST_LOG_TRIVIAL(trace) << "Completed gene score calculation for current locus.";

  return out;
}

std::vector<long double> z_transform(std::vector<Gene_score> const & scores) {

  // using high precision to avoid overflow on huge data sets
  // bit tedious but should be futureproof

  std::vector<long double> out;
  out.reserve(scores.size());
  
  using Float512 = boost::multiprecision::number<boost::multiprecision::cpp_bin_float<512>>;

  // mean
  Float512 sum = 0;
  sum = std::accumulate(std::begin(scores), std::end(scores), sum);
  long double const mean = static_cast<long double>(sum / scores.size());

  // stdev
  sum = 0;
  sum = std::inner_product(std::begin(scores), std::end(scores), std::begin(scores), sum);
  long double const sqsum_mean = static_cast<long double>(sum / scores.size());

  long double const stdev = std::sqrt(sqsum_mean - mean * mean);

  if (stdev < 0.0000001) {
    // avoid division by zero
    out.resize(scores.size(), 0);
    return out;
  }

  // z transform
  for (auto const & score : scores)
    out.push_back((score - mean) / stdev);

  return out;
}
