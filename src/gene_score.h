#ifndef INCL_GENE_SCORE_H
#define INCL_GENE_SCORE_H

#include "snpreader.h"
#include <vector>

using Gene_score = long double;

std::vector<Gene_score> gene_score(SNPreader::Locus const &, SNPreader::Locus const &);

std::vector<long double> z_transform(std::vector<Gene_score> const &);
  
#endif
