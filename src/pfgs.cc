#include "command_line_arguments.h"
#include "gzfile.h"
#include "genblock.h"
#include "plinkdosagereader.h"
#include "vcfreader.h"
#include "gene_score.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <boost/log/trivial.hpp>
#include <boost/algorithm/string/predicate.hpp> // ends_with

int main(int argc, char ** argv) {

  // parse command line arguments
  auto const global_args = command_line_arguments(argc, argv);
  double const maf_limit = global_args["maf"].as<double>();
  std::size_t const pre_flank = global_args["flank"].as<std::size_t>() ?
    global_args["flank"].as<std::size_t>() * 1000 : global_args["pre-flank"].as<std::size_t>() * 1000;
  std::size_t const post_flank = global_args["flank"].as<std::size_t>() ?
    global_args["flank"].as<std::size_t>() * 1000 : global_args["post-flank"].as<std::size_t>() * 1000;
  bool const zscore = global_args.count("zscore");
  auto const gtf_filter = create_gtf_filter(global_args);
  auto const snp_filter = create_snp_filter(global_args);
  bool const do_include_filter = global_args.count("include-snps");
  std::string const delimiter = global_args.count("delimiter") ? global_args["delimiter"].as<std::string>() : ",";
  bool const hard_calls = global_args.count("hard-calls");
  double const maf_ratio = global_args["maf-ratio"].as<double>();

  // Open output file
  std::ofstream out_file(global_args["out"].as<std::string>());
  if (!out_file) {
    std::cerr << "Can't open output file for writing. Abort.\n";
    return 1;
  }
  
  // Open sample file
  std::unique_ptr<SNPreader> sample_file;
  
  // if plink
  if (global_args.count("samples")) {
    BOOST_LOG_TRIVIAL(info) << "Assuming sample input files are plink dosage format";
    sample_file = std::unique_ptr<SNPreader>{std::make_unique<Plinkdosagereader>(global_args["samples"].as<std::vector<std::string>>())};
  }
  else if (boost::algorithm::ends_with(global_args["sample"].as<std::string>(), ".dosage") || boost::algorithm::ends_with(global_args["sample"].as<std::string>(), ".dosage.gz")) {
    BOOST_LOG_TRIVIAL(info) << "Assuming sample input file is plink dosage format";
    sample_file = std::unique_ptr<SNPreader>{std::make_unique<Plinkdosagereader>(global_args["sample"].as<std::string>())};
  }
  else {
    BOOST_LOG_TRIVIAL(info) << "Assuming sample input file is VCF format (default).";
    sample_file = std::unique_ptr<SNPreader>{std::make_unique<VCFreader>(global_args["sample"].as<std::string>(), hard_calls)};
  }
  
  // Open reference file
  auto reference_file = std::unique_ptr<SNPreader>{std::make_unique<VCFreader>(global_args["reference"].as<std::string>(), hard_calls)};
  
  // Open gene file
  GZfile gene_file(global_args["genes"].as<std::string>());
  if (!gene_file)  {
    std::cerr << "Can't open gene file for reading. Abort.\n";
    return 1;
  }

  BOOST_LOG_TRIVIAL(info) << "Outputting subject ids.";
  out_file << "gene_name" << delimiter << "gene_id" << delimiter << "chr" 
           << delimiter << "start" << delimiter << "stop" << delimiter 
           << "Nsample" << delimiter << "Nref" << delimiter << "num_loci" << delimiter << "total_num_loci";
  for (std::size_t sidx = 0; sidx != sample_file->num_samples(); ++sidx)
    out_file << delimiter << sample_file->sample_id(sidx);
  out_file << '\n';
  
  Genblock gb;
  SNPreader::Locus sample_locus, reference_locus;
  std::map<long long, std::map<std::size_t, std::vector<Gene_score>>> gscore;
  
  // initialising sample_locus
  bool do_sample_read = false, do_ref_read = true; // first read should be skipped, because it's done here
  if (!(*sample_file >> sample_locus)) {
    std::cerr << "Sample file is empty or corrupt. No sample data present. Abort.\n";
    return 1;
  }
  
  //std::map<std::size_t, std::vector<float>> raw_data_sample, raw_data_ref;

  std::vector<unsigned> ref_skipped_loci;
  long prev_chr = -1;

  while (gene_file >> gb) {
    
    BOOST_LOG_TRIVIAL(trace) << "Read " << gb << " from gene file.";
    
    if (!gb.chr || !gb.start || !gb.stop) {
      BOOST_LOG_TRIVIAL(trace) << "Incomplete information on region: skipping.";
      continue;
    }

    if (gb.chr != prev_chr)
      ref_skipped_loci.clear();

    prev_chr = gb.chr;
    
    bool pass_gtf_filter = true;
    for (auto const & f : gtf_filter) {
      auto itt = gb.attr.find(f.first);
      if (itt == gb.attr.end()) {
        BOOST_LOG_TRIVIAL(trace) << "Region does not have info for gtf_filter `" << f.first << "`: skipping\n";
        pass_gtf_filter = false;
        break;
      }
      if (itt->second != f.second) {
        BOOST_LOG_TRIVIAL(trace) << "Region fails gtf_filter `" << f.first << '=' << f.second << "` (has value `" << itt->second << "`): skipping\n";
        pass_gtf_filter = false;
        break;
      }
    }
    
    if (!pass_gtf_filter)
      continue;

    // include flanking regions  
    gb.start = gb.start < pre_flank ? 0 : gb.start - pre_flank;
    gb.stop += post_flank;
    
    if (gb.chr < sample_locus.chr || (gb.chr == sample_locus.chr && gb.stop < sample_locus.pos)) {
      //BOOST_LOG_TRIVIAL(trace) << "No sample loci within gene region: skipping";
      continue;
    }

    // Read from sample and ref until we are beyond the gene
    for (;;) {

      if (do_sample_read && !(*sample_file >> sample_locus)) {
        BOOST_LOG_TRIVIAL(trace) << "Sample file is empty or corrupt. No more sample loci will be read.";
        break;
      }
      
      BOOST_LOG_TRIVIAL(trace) << "Read sample locus " << sample_locus;
      do_sample_read = true;

      if (sample_locus.chr < gb.chr || sample_locus.pos < gb.start) {
        BOOST_LOG_TRIVIAL(trace) << "Sample locus lies in front of current gene. Discarding locus, reading next.";
        continue;
      }
      
      if (sample_locus.chr < reference_locus.chr || 
          (sample_locus.chr == reference_locus.chr && sample_locus.pos < reference_locus.pos)) {
        BOOST_LOG_TRIVIAL(trace) << "Sample locus lies in front of reference locus. Discarding locus, reading next.";
        continue;
      }
      
      // snp filter, --include-snps and --exclude-snps options
      auto const itt = snp_filter.find(sample_locus.id);
      if (itt != snp_filter.end()) {
        if (do_include_filter) {
          BOOST_LOG_TRIVIAL(trace) << "Sample locus listed in "
            "include filter: including locus.";
        }
        else {
          BOOST_LOG_TRIVIAL(trace) << "Sample locus listed in "
            "exclude filter: excluding locus.";
          continue;
        }
      }
      else {
        if (do_include_filter) {
          BOOST_LOG_TRIVIAL(trace) << "Sample locus not listed in "
            "include filter: excluding locus.";
            continue;
        }
        else {
          BOOST_LOG_TRIVIAL(trace) << "Sample locus not listed in "
            "exclude filter: including locus.";
        }
      }

      if (sample_locus.chr > gb.chr || (sample_locus.chr == gb.chr && sample_locus.pos > gb.stop)) {
        BOOST_LOG_TRIVIAL(trace) << "Sample locus lies after current gene. Stop reading loci.";
        do_sample_read = false;
        break;  // without do_sample_read, this line would cause the current locus to be ignored
      }
      
      while (!do_ref_read || *reference_file >> reference_locus) {
        BOOST_LOG_TRIVIAL(trace) << "Read reference locus "
          << reference_locus;
          
        if (do_ref_read && reference_locus.chr == gb.chr && reference_locus.pos >= gb.start && reference_locus.pos <= gb.stop)
          ref_skipped_loci.push_back(reference_locus.pos);

        if (reference_locus.chr > sample_locus.chr || (reference_locus.chr == sample_locus.chr && reference_locus.pos > sample_locus.pos)) {
          BOOST_LOG_TRIVIAL(trace) << "  Reference locus lies after current sample locus. Keeping ref locus, reading next sample locus.";
          do_ref_read = false;
          break;
        }

        do_ref_read = true;
        
        if (reference_locus.chr < sample_locus.chr || (reference_locus.chr == sample_locus.chr && reference_locus.pos < sample_locus.pos)) {
          BOOST_LOG_TRIVIAL(trace) << "  Reference locus lies before sample locus. Discarding ref locus, reading next.";
          continue;
        }
        
        // sample and ref locus match:
        break;
      }
      
      if (!*reference_file) {
        BOOST_LOG_TRIVIAL(trace) << "Reference file is empty or corrupt. No more reference loci will be read.";
        break; // reference file is empty/corrupt TODO remove?
      }
      
      if (reference_locus.chr != sample_locus.chr || reference_locus.pos != sample_locus.pos) {
        BOOST_LOG_TRIVIAL(trace) << "Current sample locus not found in reference. Skipping sample locus.";
        do_ref_read = false; // duplicate, not needed but for clarity
        continue;
      }
      
      BOOST_LOG_TRIVIAL(trace) << "Current sample locus found in reference.";
      
      // check for mismatch / flip
      sample_locus.parse_alt();
      reference_locus.parse_alt();
      if (sample_locus.ref != reference_locus.ref || sample_locus.palt.empty() || reference_locus.palt.empty() || sample_locus.palt[0] != reference_locus.palt[0]) {
        if (!sample_locus.palt.empty() && !reference_locus.palt.empty() && sample_locus.ref == reference_locus.palt[0] && reference_locus.ref == sample_locus.palt[0]) {
          BOOST_LOG_TRIVIAL(info) << "Sample has opposite alt/ref from reference:"
            << "S: " << sample_locus << "\nR: " << reference_locus << '\n' << "Attempting to correct...";
          sample_locus.switch_alt_ref();
        }
        else {
          BOOST_LOG_TRIVIAL(info) << "Sample and reference for locus have different alt/ref allele:\n"
            << sample_locus << '\n' << reference_locus << '\n' << "Skipping locus.";
          continue;
        }
      }
      
      BOOST_LOG_TRIVIAL(trace) << "mismatch check done";
        
      // Do deep read for sample. This is the computationally expensive part.
      sample_file->deep_read(sample_locus);
      if (sample_locus.maf < maf_limit) {
        BOOST_LOG_TRIVIAL(trace) << "Sample locus failed MAF gtf_filter with MAF=" << sample_locus.maf << ". Skipping.";
        sample_locus.clear();
        continue;
      }

      BOOST_LOG_TRIVIAL(trace) << "deep read sample done";
      
      reference_file->deep_read(reference_locus);
      if (reference_locus.maf < maf_limit) {
        BOOST_LOG_TRIVIAL(trace) << "Reference locus failed MAF gtf_filter with MAF=" << reference_locus.maf << ". Skipping.";
        sample_locus.clear();
        reference_locus.clear();
        continue;
      }

      BOOST_LOG_TRIVIAL(trace) << "deep read reference done";
      
      sample_locus.switch_ar = false; // reset flag
 
      // Calculate scores
      gscore[sample_locus.chr][sample_locus.pos] = gene_score(sample_locus, reference_locus);
      
      
      // save raw data
      //raw_data_sample[sample_locus.pos] = sample_locus.data_ds;
      //raw_data_ref[reference_locus.pos] = reference_locus.data_ds;
      // end save raw data
      
      BOOST_LOG_TRIVIAL(trace) << "GS calc done";
    }
    
    // MAKE SURE THE REFERENCE FILE IS UP TO DATE
      while (!do_ref_read || *reference_file >> reference_locus) {
        BOOST_LOG_TRIVIAL(trace) << "Read/update reference locus "
          << reference_locus;
          
        if (do_ref_read && reference_locus.chr == gb.chr && reference_locus.pos >= gb.start && reference_locus.pos <= gb.stop)
          ref_skipped_loci.push_back(reference_locus.pos);

        if (reference_locus.chr > sample_locus.chr || (reference_locus.chr == sample_locus.chr && reference_locus.pos > sample_locus.pos)) {
          BOOST_LOG_TRIVIAL(trace) << "  Update reference locus lies after current sample locus. Keeping ref locus, reading next sample locus.";
          do_ref_read = false;
          break;
        }

        do_ref_read = true;
        
        if (reference_locus.chr < sample_locus.chr || (reference_locus.chr == sample_locus.chr && reference_locus.pos < sample_locus.pos)) {
          BOOST_LOG_TRIVIAL(trace) << "  Update reference locus lies before sample locus. Discarding ref locus, reading next.";
          continue;
        }
        
        // sample and ref locus match:
        break;
      }

    // remove old reference ref loci
    ref_skipped_loci.erase(std::begin(ref_skipped_loci), std::lower_bound(std::begin(ref_skipped_loci), std::end(ref_skipped_loci), gb.start));
    auto const upper_limit_rl = std::lower_bound(std::begin(ref_skipped_loci), std::end(ref_skipped_loci), gb.stop);
    auto const total_loci = std::distance(std::begin(ref_skipped_loci), upper_limit_rl);

    auto lower_bound = gscore[gb.chr].lower_bound(gb.start);
    
    if (lower_bound == gscore[gb.chr].end()) {
      BOOST_LOG_TRIVIAL(trace) << "No loci found for current gene. Skipping.";
      continue;
    }

    gscore[gb.chr].erase(gscore[gb.chr].begin(), lower_bound); // erase all old scores
    
/*

    // erase old raw data
    raw_data_sample.erase(raw_data_sample.begin(), raw_data_sample.lower_bound(gb.start));
    raw_data_ref.erase(raw_data_ref.begin(), raw_data_ref.lower_bound(gb.start));
    // end erase old raw data
    
    // print raw data
    std::ofstream fout_gene(gb.attr["gene_id"] + ".gene.csv");
    std::ofstream fout_sample(gb.attr["gene_id"] + ".sample.csv");
    std::ofstream fout_ref(gb.attr["gene_id"] + ".ref.csv");
    fout_gene << gb.attr["gene_name"] << delimiter << gb.attr["gene_id"] << delimiter << gb.chr << delimiter << gb.start << delimiter << gb.stop << '\n';
    for (auto & snp : raw_data_sample) {
      fout_sample << snp.first;
      for (auto & ds : snp.second)
        fout_sample << delimiter << ds;
      fout_sample << '\n';
    }
    for (auto & snp : raw_data_ref) {
      fout_ref << snp.first;
      for (auto & ds : snp.second)
        fout_ref << delimiter << ds;
      fout_ref << '\n';
    }
    // end print raw data

*/

    auto const upper_bound = gscore[gb.chr].upper_bound(gb.stop);
    std::size_t const num_loci = std::distance(lower_bound, upper_bound);
    
    BOOST_LOG_TRIVIAL(trace) << "Found " << num_loci << " loci for current gene.";

    std::vector<Gene_score> total_gscore;
    total_gscore.resize(sample_file->num_samples(), 0);
    while (lower_bound != upper_bound) {
      BOOST_LOG_TRIVIAL(trace) << " - " << gb.chr << ':' << lower_bound->first;

      // add score for current locus to total
      for (std::size_t sample_idx = 0; sample_idx != total_gscore.size(); ++sample_idx) {
        total_gscore[sample_idx] += lower_bound->second[sample_idx];
      }

      ++lower_bound;
    }

    BOOST_LOG_TRIVIAL(info) << "Outputting gene scores for " << gb.attr["gene_id"] << " (" << gb.chr << ':' << gb.start << '-' << gb.stop << ") based on "  << num_loci << " loci.";

    out_file << gb.attr["gene_name"] << delimiter << gb.attr["gene_id"] << delimiter << gb.chr << delimiter 
             << gb.start << delimiter << gb.stop << delimiter << sample_file->num_samples() << delimiter << reference_file->num_samples() << delimiter << num_loci << delimiter << total_loci;
    if (zscore) {
      for (auto const & score : z_transform(total_gscore))
        out_file << delimiter << score;
    }
    else {
      for (auto const & score : total_gscore)
        out_file << delimiter << score / (2 * num_loci * reference_file->num_samples());
    }
    out_file << '\n';
  }
  
  std::cout << "Program completed.\n";
}
